FROM article714/debian-based-container:0.8.0
LABEL maintainer="C. Guychard @ Article714"


# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8
# directory that contains service configurations
ENV SVDIR /container/services

# Container Arguments
ARG KEYCLOAK_VERSION
ARG KEYCLOAK_DIST=https://github.com/keycloak/keycloak/releases/download/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz

ENV JDBC_POSTGRES_VERSION 42.2.5

# Container tooling

COPY container /container

# container building

RUN /container/build.sh

# LOGS  in external volume
VOLUME /var/log
# Configuration  in external volume
VOLUME /container/config

# entrypoint  & default command
ENTRYPOINT [ "/container/entrypoint.sh" ]
CMD ["start"]

# healthcheck
HEALTHCHECK --interval=120s --timeout=2s --retries=5 CMD /container/check_running