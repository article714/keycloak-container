#!/bin/bash
#
# part of this script has been extracted from    https://github.com/keycloak/keycloak-containers (Apache License)
#

set -x

export LANG=en_US.utf8

# install dependencies
mkdir -p /usr/share/man/man1
apt-get update
apt-get install -qy hostname openjdk-11-jdk-headless openssl tar debianutils curl

# Add Keycloak user
groupadd keycloak
adduser --no-create-home --disabled-password --shell /usr/sbin/nologin --gecos "" --ingroup keycloak keycloak

# Downloaod Keycloak
echo "Keycloak from [download]: $KEYCLOAK_DIST"
cd /tmp
curl -L $KEYCLOAK_DIST | tar zvx
mv /tmp/keycloak-* /opt/keycloak
chown -R keycloak. /opt/keycloak
mv /opt/keycloak/themes /container/config/keycloak
cd /opt/keycloak
ln -s /container/config/keycloak/themes themes

# Postgresql JDBC

mkdir -p /opt/keycloak/modules/system/layers/base/org/postgresql/jdbc/main
cd /opt/keycloak/modules/system/layers/base/org/postgresql/jdbc/main
curl -L https://repo1.maven.org/maven2/org/postgresql/postgresql/$JDBC_POSTGRES_VERSION/postgresql-$JDBC_POSTGRES_VERSION.jar >postgres-jdbc.jar
cp /container/data/pg_jdbc_module.xml ./module.xml

#--
# configuration

cd /opt/keycloak/standalone
ls -alrt /container/config/keycloak
mv configuration/* /container/config/keycloak
rm -Rf configuration
ln -s /container/config/keycloak configuration
ls -alrt /container/config/keycloak
cd ..
bin/jboss-cli.sh --file=/container/data/default-configuration.cli
rm -rf /container/config/keycloak/standalone_xml_history
cd /
ls -alrt /container/config/keycloak

#--
# permissions

chown -R keycloak.root /opt/keycloak
chown -R keycloak.root /container/config/keycloak
chmod -R g+rwX /opt/keycloak
chmod -R g+rwX /container/config/keycloak
touch /var/log/jboss.log
chown keycloak.syslog /var/log/jboss.log
touch /var/log/keycloak.log
chown keycloak.syslog /var/log/keycloak.log

#--
# Cleaning
apt-get -yq clean
apt-get -yq autoremove
rm -rf /var/lib/apt/lists/*
rm -rf /opt/keycloak/standalone/tmp/auth
rm -rf /opt/keycloak/domain/tmp/auth
rm -rf /tmp/*
rm -rf /var/log/apt/*

# truncate logs
truncate --size 0 /var/log/lastlog
truncate --size 0 /var/log/faillog
truncate --size 0 /var/log/dpkg.log
truncate --size 0 /var/log/syslog
truncate --size 0 /var/log/jboss.log
truncate --size 0 /var/log/keycloak.log

# plop
cd /
