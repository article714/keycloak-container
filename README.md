# keycloak-container

A container used to deploy a Keycloak on a Docker infrastructure

_This work has been inspired by and includes elements from [Keycloak-containers project](https://github.com/keycloak/keycloak-containers)_
